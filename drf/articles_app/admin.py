from django.contrib import admin
from .models import SavedArticles

# Register your models here.
admin.site.register(SavedArticles)