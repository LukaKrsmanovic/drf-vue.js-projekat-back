from ..models import SavedArticles
from rest_framework import serializers


class SavedArticlesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = SavedArticles
        fields = "__all__"