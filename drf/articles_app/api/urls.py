from django.urls import path
from .views import (SectionList, SubsectionList, 
                    DailyArticleCount, AuthorArticles, ArticleList, 
                    SavedArticlesList)

urlpatterns = [
    path('sections/', SectionList.as_view(), name='section-list'),
    path('section/subsections/', SubsectionList.as_view(), name='subsection-list'),
    path('section/subsection/articles/', ArticleList.as_view(), name='article-list'),
    path('section/subsection/daily_article_count/', DailyArticleCount.as_view(), name='daily-article-count'),
    path('section/subsection/author_article_count/', AuthorArticles.as_view(), name='author-article-count'),
    
    path('saved_list/', SavedArticlesList.as_view(), name='user-articles-list'),
]