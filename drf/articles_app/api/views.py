from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from ..models import SavedArticles
from .serializers import SavedArticlesSerializer

from .functions import add_pagination, fill_in_days, get_json_response

# url example https://api.nytimes.com/svc/archive/v1/2019/1.json?api-key=DVWZPeDTh5YOPpPMWzm4BimnIYaVrwy3

base_url = 'https://api.nytimes.com/svc/archive/v1/'
api_key = 'DVWZPeDTh5YOPpPMWzm4BimnIYaVrwy3'

use_local = True


class SectionList(APIView):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        date = request.data['date']
        
        year = date[:4]
        month = date[5:7].replace('-', '')
        
        if month[0] == '0':
            month = month[1]
        
        url = f'{base_url}{year}/{month}.json?api-key={api_key}'
        
        data = get_json_response(url, use_local)
        
        return_data = {
            'sections': [],
        }
        
        for article in data['response']['docs']:
            return_data['sections'].append(article['section_name'])
        
        return_data['sections'] = set(return_data['sections'])
        return Response(return_data)


class SubsectionList(APIView):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        date = request.data['date']
        section = request.data['section']
        
        year = date[:4]
        month = date[5:7].replace('-', '')
        
        if month[0] == '0':
            month = month[1]
        
        url = f'{base_url}{year}/{month}.json?api-key={api_key}'
        
        data = get_json_response(url, use_local)
        
        return_data = {
            'section': section,
            'subsections': [],
        }
        
        for article in data['response']['docs']:
            if article['section_name'] == section:
                try:
                    return_data['subsections'].append(article['subsection_name'])
                except:
                    pass
        
        return_data['subsections'] = set(return_data['subsections'])
        return Response(return_data)


class ArticleList(APIView):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        try:      
            page = int(request.query_params.get('page', 1))
            limit = int(request.query_params.get('limit', 10))
        
            page_url = 'articles/section/subsection/articles/'
        
            date = request.data['date']
            section = request.data['section']
            subsection = request.data['subsection']
            
            year = date[:4]
            month = date[5:7].replace('-', '')
            
            if month[0] == '0':
                month = month[1]
            
            url = f'{base_url}{year}/{month}.json?api-key={api_key}'
            
            data = get_json_response(url, use_local)
            
            return_data = {
                'date': date,
                'section': section,
                'subsection': subsection,
                'page': page,
                'total_pages': None,
                'prev_page': None,
                'next_page': None,
                'articles': [],
            }
            
            saved_articles = SavedArticles.objects.all()
            serializer = SavedArticlesSerializer(saved_articles, many=True)
            
            saved_articles = list(filter(
                lambda article: article['reader_id'] == request.user.id, serializer.data
            )) 
            
            for article in data['response']['docs']:
                try:
                    if article['section_name'] == section and \
                        article['subsection_name'] == subsection:
                            
                            # remove By at the beginning, repalace and with ,
                            if len(article['byline']['original']) > 3:
                                article['byline']['original'] = article['byline']['original'][3:]
                                article['byline']['original'] = article['byline']['original'].replace(' and ', ', ')
                                
                            favorite = False
                            if any(obj['headline_main'] == article['headline']['main'] for obj in saved_articles):
                                favorite = True
                                  
                            return_data['articles'].append({
                                'favorite': favorite,
                                'headline_main': article['headline']['main'],
                                'abstract': article['abstract'],
                                'web_url': article['web_url'],
                                'lead_paragraph': article['lead_paragraph'],
                                'pub_date': article['pub_date'],
                                'word_count': article['word_count'],
                                'byline': article['byline']['original']
                            })
                except:
                    pass
            
            return_data['articles'], status = add_pagination(return_data, page_url, page, limit)
        
            if status == 404:
                return Response({
                    'error': 'This page doesn\'t exist'
                }, status=404)
            
            return Response(return_data)
        
        except:
            return Response({
                'error': 'API not working, try again'
            }, status=400)


class DailyArticleCount(APIView):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        date = request.data['date']
        section = request.data['section']
        subsection = request.data['subsection']
        
        year = date[:4]
        month = date[5:7].replace('-', '')
        
        if month[0] == '0':
            month = month[1]
        
        url = f'{base_url}{year}/{month}.json?api-key={api_key}'
        
        data = get_json_response(url, use_local)
        
        return_data = {
            'date': date,
            'section': section,
            'subsection': subsection,
            'daily_article_count': [],
        }
        
        for article in data['response']['docs']:
            try:
                if article['section_name'] == section and \
                    article['subsection_name'] == subsection:
                        day = article['pub_date'][8:10]
                        if not any(obj['x'] == day for obj in return_data['daily_article_count']):
                            return_data['daily_article_count'].append({
                                'x': day, 
                                'y': 1
                            })
                        else:
                            obj = list(
                                filter(lambda l : l['x'] == day, return_data['daily_article_count'])
                            )[0]
                            index = return_data['daily_article_count'].index(obj)
                            
                            return_data['daily_article_count'][index]['y'] += 1
            except:
                pass
        
        return_data['daily_article_count'] = fill_in_days(
            year, month, return_data['daily_article_count']
        )
        
        return_data['daily_article_count'].sort(key = lambda x : x['x'])
        return Response(return_data)


class AuthorArticles(APIView):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        date = request.data['date']
        section = request.data['section']
        subsection = request.data['subsection']
        
        year = date[:4]
        month = date[5:7].replace('-', '')
        
        if month[0] == '0':
            month = month[1]
        
        url = f'{base_url}{year}/{month}.json?api-key={api_key}'
        
        data = get_json_response(url, use_local)
        
        return_data = {
            'date': date,
            'section': section,
            'subsection': subsection,
            'author_article_count': [],
        }
        
        for article in data['response']['docs']:
            try:
                if article['section_name'] == section and \
                    article['subsection_name'] == subsection:
                        authors = article['byline']['person']
                        for author in authors:
                            auth_name = author['firstname'] + ' ' + author['lastname']
                            if not any(obj['x'] == auth_name for obj in return_data['author_article_count']):
                                return_data['author_article_count'].append({
                                    'x': auth_name, 
                                    'y': 1
                                })
                            else:
                                obj = list(
                                    filter(lambda l : l['x'] == auth_name, return_data['author_article_count'])
                                )[0]
                                index = return_data['author_article_count'].index(obj)
                                
                                return_data['author_article_count'][index]['y'] += 1
            except:
                pass
            
        return Response(return_data)


class SavedArticlesList(APIView):
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        page = int(request.query_params.get('page', 1))
        limit = int(request.query_params.get('limit', 2))
        
        page_url = 'articles/saved_list/'
        
        return_data = {
            'page': page,
            'total_pages': None,
            'prev_page': None,
            'next_page': None,
            'articles': [],
        }
            
        saved_articles = SavedArticles.objects.all()
        serializer = SavedArticlesSerializer(saved_articles, many=True)
        
        return_data['articles'] = list(filter(
            lambda article: article['reader_id'] == request.user.id, serializer.data
        )) 
        return_data['articles'], status = add_pagination(return_data, page_url, page, limit)
        
        if status == 404:
            return Response({
                'error': 'This page doesn\'t exist'
            }, status=404)
        
        return Response(return_data)
    
    def post(self, request):        
        request.data.update({"reader_id": request.user.id})
        
        serializer = SavedArticlesSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
        
    def delete(self, request):
        try:
            saved_article = SavedArticles.objects.get(pk = int(request.data['id']))
            serializer = SavedArticlesSerializer(saved_article)
            
            if serializer.data['reader_id'] == request.user.id:
                saved_article.delete()
            else:
                return Response({
                    'error': 'There is no such article for current user'
                }, status=400)
        except:
            return Response({
                'error': 'Saved article with this id does not exist'
            }, status=400)
        
        page = 1
        try:
            page = int(request.data['page'])
        except:
            pass
        limit = int(request.query_params.get('limit', 2))
        
        page_url = 'saved_list/'
        
        return_data = {
            'page': page,
            'total_pages': None,
            'prev_page': None,
            'next_page': None,
            'articles': [],
        }
        saved_articles = SavedArticles.objects.all()
        serializer = SavedArticlesSerializer(saved_articles, many=True)
        
        return_data['articles'] = list(filter(
            lambda article: article['reader_id'] == request.user.id, serializer.data
        )) 
        
        return_data['articles'], status = add_pagination(return_data, page_url, page, limit, 'delete')
        
        if status == 404:
            return Response({
                'error': 'This page doesn\'t exist'
            }, status=404)
        
        return Response(return_data)
    