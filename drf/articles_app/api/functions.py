import math, json, requests


def add_pagination(return_data, url, page, limit, req_type = ''):
    return_data['total_pages'] = math.ceil(len(return_data['articles'])/limit)
    
    if return_data['total_pages'] == 0:
        page = 1
    elif page < return_data['total_pages']:
        return_data['next_page'] = f'{url}?page={page+1}'
    elif page > return_data['total_pages'] and req_type == 'delete':
        page = return_data['total_pages']
    elif page > return_data['total_pages']:
        return [], 404
    
    if return_data['total_pages'] == 0:
        page = 1
    elif page > 1:
        return_data['prev_page'] = f'{url}?page={page-1}'
    elif page < 1:
        return [], 404
    
    return_data['page'] = page
        
    return return_data['articles'][((page-1) * limit) : (page * limit)], 200


def fill_in_days(year, month, daily_article_count):
    year = int(year)
    month = int(month)
    
    total_days = 31
    
    if (month % 2 == 0 and month < 7) \
        or (month % 2 != 0 and month > 8):
            total_days = 30
            
    if month == 2:
        total_days = 28
        if year % 4 == 0:
            total_days += 1
    
    for day in range(total_days):
        day_str = f'0{day+1}' if day+1 < 10 else str(day+1)
        if not any(obj['x'] == day_str for obj in daily_article_count):
            daily_article_count.append({
                'x': day_str,
                'y': 0
            })
    
    return daily_article_count


def get_json_response(url, use_local = False):
    
    if use_local:
        file = open('articles_test.json')
        data = json.load(file)
        file.close()
        return data
    
    res = requests.get(url)
    return res.json()
