from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class SavedArticles(models.Model):
    reader_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_saved_articles')
    headline_main = models.CharField(max_length=300)
    abstract = models.CharField(max_length=300)
    web_url = models.CharField(max_length=300)
    lead_paragraph = models.TextField()
    pub_date = models.CharField(max_length=50)
    word_count = models.IntegerField()
    byline = models.CharField(max_length=300, blank=True)
    
    def __str__(self):
        return self.headline_main
    