from .serializers import RegistrationSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

class RegistrationView(APIView):
    
    def post(self, request):
        serializer = RegistrationSerializer(data = request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        
        return Response(serializer.errors, status=400)
