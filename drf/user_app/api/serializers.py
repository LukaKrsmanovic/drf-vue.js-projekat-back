from django.contrib.auth.models import User
from rest_framework import serializers

class RegistrationSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(
        style={'input_type': 'password'}, 
        write_only=True
    )
    
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password', 'confirm_password']
        extra_kwargs = {
            'password': {'write_only': True}
        }
        
    def save(self):
        password = self.validated_data['password']
        confirm_password = self.validated_data['confirm_password']
        
        if password != confirm_password:
            raise serializers.ValidationError({
                'error': 'Password and Confirm Password don\'t match'
            })
        
        if User.objects.filter(email = self.validated_data['email']).exists():
            raise serializers.ValidationError({
                'error': 'This email already exists'
            })
            
        if User.objects.filter(username = self.validated_data['username']).exists():
            raise serializers.ValidationError({
                'error': 'This username already exists'
            })
            
        if len(self.validated_data['first_name']) < 2 \
            or len(self.validated_data['last_name']) < 2 \
            or len(self.validated_data['username']) < 2:  
                raise serializers.ValidationError({
                    'error': 'First name, last name and username must have 3 or more characters'
                })
            
        account = User(
            email = self.validated_data['email'],
            username = self.validated_data['username'],
            first_name = self.validated_data['first_name'],
            last_name = self.validated_data['last_name']
        )
        account.set_password(password)
        account.save()
        
        return account