# DRF + Vue.js projekat - Back



## Possible mysqlclient error and solution (Ubuntu 20.04)
Trying to ``` pip install mysqlclient ``` might result in an error. <br />
To fix it follow these steps:

- Step 0: ``` sudo apt install python3-dev build-essential ```
- Step 1: ``` sudo apt install libssl1.1 ```
- Step 2: ``` sudo apt install libssl1.1=1.1.1f-1ubuntu2 ```
- Step 3: ``` sudo apt install libssl-dev ```
- Step 4: ``` sudo apt install libmysqlclient-dev ```

After this try installing mysqlclient with pip command <br />
``` pip install mysqlclient ```

